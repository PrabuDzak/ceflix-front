# Front End Ceflix

### Folder
- **development**
  
    Semua pekerjaan dilakukan disini

- **production** 
  
    buat target build

- **node_modules**

    BARANG MEJIK tidak perlu tau

## Cara ngebuild

### Requirement
1. [Jekyll](http://jekyllrb.com/) - `$ gem install jekyll`
2. [NodeJS](http://nodejs.org) - use the installer.
3. [GulpJS](https://github.com/gulpjs/gulp) - `$ npm install -g gulp`

### Installation
1. cd to project root
2. run `npm install`

### Usage
1. run `gulp`

![GIF](http://f.cl.ly/items/373y2E0e0i2p0E2O131g/test-gif.gif)