tar -czvf nos.tar.gz production --exclude='storage'
scp nos.tar.gz high@10.122.1.175:/var/www/ceflix.com/
ssh high@10.122.1.175 'cd /var/www/ceflix.com/; tar -xzvf nos.tar.gz; mv -f --backup=numbered production/** public_html'
rm -rf nos.tar.gz
