console.log('ceflix');

var ceflix = angular.module('ceflix', [], function($interpolateProvider){
  $interpolateProvider.startSymbol('[[');
  $interpolateProvider.endSymbol(']]');
})

ceflix.config(['$locationProvider', function($locationProvider) {
  $locationProvider.html5Mode({
    enabled: true,
    requireBase: false,
    rewriteLinks: false
  });
}]);

ceflix.filter('trustUrl', ['$sce', function($sce){
  return function(recordingUrl){
    return $sce.trustAsResourceUrl(recordingUrl);
  }
}]);

ceflix.controller('HomeController', function ($http){
  var home = this;
  
  $http.get('/api/movie_list.php')
    .then(function(resp){
      home.filmList = resp.data;
      console.log(resp.data);
    });

});

ceflix.controller('MovieWatcher', function($location, $http){
  var mw = this;

  var url = $location.search();
  
  $http.get('/api/movie_request.php?movie_id=' + url.movie_id)
    .then(function(response){
      mw.info = response.data;
      mw.info.videoUrl = '/storage/movie/' + mw.info.id + '/movie.mp4';
      console.log(response);
    });
});

ceflix.controller('MovieUploader', function($http){
  var mu  = this;  

  // STUB  
  mu.formData = { };

  mu.processForm = function(){
    $http({
      method: 'POST',
      url: '/api/movie_upload.php',
      data: mu.formData,
      header: { 'Content-Type' : 'multipart/form-data' }
    })
    .then(function(response){

    },function(response){

    });
  }
});