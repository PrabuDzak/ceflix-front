var Toaster = (function () {
  function Toaster($wrapper, option) {
    if (option === void 0) { option = {}; }
    this.option = {};
    this.$wrapper = $wrapper;
    this.option.time = option.time ? option.time : 3000;
    this.option.speed = option.speed ? option.speed : 100;
    this.option.removeOnClick = option.removeOnClick ? option.removeOnClick : true;
    if(!this.$wrapper.hasClass("toaster")){
      this.$wrapper.addClass("toaster");
    }
  }
  Toaster.prototype.toast = function (message) {
    console.log('toast: ' + message);
    var $elem = $('<div>').addClass('toast');
    var $text = $('<div>').addClass('text').text(message).appendTo($elem);
    if (this.option.removeOnClick) {
      $elem.on('click', function () {
        $(this).remove();
      });
    }
    setTimeout(function () {
      $elem.addClass('remove');
    }, this.option.time);
    setTimeout(function () {
      $elem.remove();
    }, this.option.time + 250);
    this.$wrapper.prepend($elem);
  };
  return Toaster;
}());

// HOW-TO
// var wrap = $('<div>').appendTo('body'); 
// var toaster = new Toaster(wrap, { time: 5000 });
// toaster.toast("contoh message");