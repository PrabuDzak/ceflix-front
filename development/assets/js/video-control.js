
var main = document.getElementById('video-container');
var video = document.getElementById('video-source');

var videoControl = document.getElementById('video-control');

var playButton = document.getElementById('button--play-pause');
var currentTime = document.getElementById('current-time');
var durationTime = document.getElementById('duration-time'); 
var seekBar = document.getElementById('seek-bar');
var fullscreenButton = document.getElementById('button--fullscreen');
var subtitlesBar = document.getElementById('video-subtitle');

/**
 * Video
 */
var movieInfo;
var subProvider;

angular.element(document).ready(function(){

  function start(){
    var movieInfoLoaded = angular.element(document.getElementById('controller--movie-watcher')).scope().mw.hasOwnProperty('info');
    if(!movieInfoLoaded){
      window.setTimeout(start, 1000);
      return;
    }

    movieInfo = angular.element(document.getElementById('controller--movie-watcher')).scope().mw.info;
    if(movieInfo.subtitles.length > 0){
      subProvider = new SubtitleProvider(movieInfo.subtitles[0].subtitle);
    }
  }
  start();
});
video.addEventListener('click', playToggle);
video.addEventListener('timeupdate', videoUpdate);

/**
 * Subtitles
 */

var SubtitleProvider = (function () {
    function SubtitleProvider(jsonObject) {
        this.subArray = jsonObject;
        this.subIndex = 0;
    }
    SubtitleProvider.prototype.getSubtitleAt = function (time) {
        // IF TIME IS IN RANGE OF CURRENT INDEX SUBARRAY TIME
        // RETURN SUBTITLE NORMALY

        // ELSE IF TIME IS > CURRENT END TIME
        // INCREMENT INDEX
        // RETURN NULL

        // ELSE IF TIME IS < CURRENT START TIME AND TIME IS > PREV TIME END
        // WAIT
        // RETURN NULL

        // ELSE IF TIME IS OUT OF RANGE
        // SEARCH, SET INDEX TO NEAREST BEFORE
        var startTime = this.reverseTimeFormat(this.subArray[this.subIndex].startTime);
        var endTime = this.reverseTimeFormat(this.subArray[this.subIndex].endTime);
        var prevStartTime = this.reverseTimeFormat(this.subArray[Math.max(0, this.subIndex - 1)].startTime);
        var prevEndTime = this.reverseTimeFormat(this.subArray[Math.max(0, this.subIndex - 1)].endTime);
        var nextStartTime = this.reverseTimeFormat(this.subArray[Math.max(0, this.subIndex + 1)].startTime);
        var nextEndTime = this.reverseTimeFormat(this.subArray[Math.max(0, this.subIndex + 1)].endTime);
        if (startTime < time && time < endTime) {
            return this.subArray[this.subIndex].text;
        }
        else if (time > endTime && time < nextStartTime) {
            this.subIndex++;
        }
        else if (time < startTime && time > prevEndTime) {
        }
        else {
            this.subIndex = this.searchIndex(time);
        }
        return null;
    };
    SubtitleProvider.prototype.searchIndex = function (time) {
        for (var i = 1; i < this.subArray.length; ++i) {
          var currentSub = this.subArray[i];
          if (time < this.reverseTimeFormat(currentSub.startTime)) {
              return i - 1;
          }
        }
        return -2;
    };
    SubtitleProvider.prototype.reverseTimeFormat = function (time) {
        var t = time.split(',');
        var ms = parseInt(t[1]);
        t = t[0].split(':');
        var h = parseInt(t[0]) * 3600;
        var m = parseInt(t[1]) * 60;
        var s = parseInt(t[2]);
        return h + m + s + ms / 1000;
    };
    return SubtitleProvider;
}());


/**
 * Video Player
 */

playButton.addEventListener('click', playToggle);

fullscreenButton.addEventListener('click', function(){
  if(video.requestFullscreen){
    video.requestFullscreen();
  }
  else if (video.mozRequestFullScreen){
    video.mozRequestFullScreen();
  }
  else if (video.webkitRequestFullScreen){
    video.webkitRequestFullScreen();
  }
});

seekBar.addEventListener('change', function(){
  video.currentTime = video.duration * (seekBar.value / 100);
});


/**
 * Function
 */

function playToggle(){
  durationTime.innerHTML = timeFormat(video.duration).formatted;
  if(video.paused){
    video.play();
  }
  else{
    video.pause();
  }
  playButton.classList.toggle('play');
  playButton.classList.toggle('pause');
}

function timeFormat(time){
  time = parseInt(time);
  var hour   = Math.floor(time / 3600);
  var minute = Math.floor(time / 60) % 60;
  var second = Math.floor(time) % 60;
  return {
    'hour': hour,
    'minute': minute,
    'second': second,
    'formatted': (hour > 0 ? (('0' + hour).slice(-2) + ':') : '') + 
                 ('0' + minute).slice(-2) + ':' + 
                 ('0' + second).slice(-2)
  }
}

function videoStop(){
  video.currentTime = 0;
  video.pause();
  playButton.classList.add('play');
  playButton.classList.remove('pause');
}

function videoUpdate(){
  currentTime.innerText = timeFormat(video.currentTime).formatted;
  seekBar.value = (video.currentTime / video.duration * 100).toString();

  subtitlesBar.innerHTML = '';
  if(subProvider != null){
    if(subProvider.getSubtitleAt(video.currentTime) != null){
      subtitlesBar.style.display = 'block';
      var subs = subProvider.getSubtitleAt(video.currentTime);
      for(var i = 1; i < subs.length; i++ ){
        subtitlesBar.innerHTML += subs[i] + ' ';
      }
    }
    else{
      subtitlesBar.style.display = 'none';
    }
  }

  if(seekBar.value >= 100){
    videoStop();
  }
}