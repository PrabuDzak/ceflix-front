<?php

require_once('src/mysql_object.php');
require_once('src/uploader.php');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if(isset($_POST['submit'])){
        $id = $mysqli->query("SELECT MAX(id) as max from tb_movie")->fetch_assoc()['max'];
        $id = intval($id) + 1;
        $title = $_POST['movie--title'];
        $r18 = isset($_POST['movie--r18']) ? $_POST['movie--r18'] : '';

        if(isset($_FILES['movie']) && !empty($_FILES['movie'])){
            $callback = new MovieUploadStrategy($id, $title, $r18);
            $uploader = new Uploader($_FILES['movie']);

            $uploader->setAllowedMime(array('mp4'));
            $uploader->setFilename('movie');
            $uploader->setDestination(join(DIRECTORY_SEPARATOR, array('storage', 'movie', $callback->getId())));
            $uploader->setCallback($callback);
            $uploader->upload();
            
            if(isset($_FILES['movie--thumbnail']) && !empty($_FILES['movie--thumbnail'])){
                $uploaderThumbnail = new Uploader($_FILES['movie--thumbnail']);

                $uploaderThumbnail->setAllowedMime(array('jpg', 'jpeg', 'png'));
                $uploaderThumbnail->setFilename('thumbnail');
                $uploaderThumbnail->setDestination(join(DIRECTORY_SEPARATOR, array('storage', 'movie', $callback->getId())));
                $uploaderThumbnail->upload();
            }
        }
    }
}

header( "refresh:5;url=/" ); 
echo 'You\'ll be redirected in about 1 secs. If not, click <a href="/">here</a>.'; 

exit();

?>