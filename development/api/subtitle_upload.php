<?php

require_once('src/mysql_object.php');
require_once('src/uploader.php');
require_once('src/srt2json_converter.php');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if(isset($_POST['submit'])){
        if(!empty($_FILES['subtitle'])){
            $movieId = 2;
            $languageId = 3;
            $converter = new SRTConverter($_FILES['subtitle']['tmp_name']);
            $callback = new SubtitleUploadStrategy($movieId, $languageId, $converter);

            $uploader = new Uploader($_FILES['subtitle']);
            $uploader->setAllowedMime(array('srt'));
            $uploader->setFilename('subtitle');
            $uploader->setDestination(join(DIRECTORY_SEPARATOR, array('storage', 'movie', $movieId)));
            $uploader->setCallback($callback);
            if($uploader->upload()){
                echo "jos";
            }
        }
        else{
            echo 'empty';
        }
    }
}

?>