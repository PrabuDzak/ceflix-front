<?php

require_once('src/mysql_object.php');

$query = "SELECT id, series_id AS seriesId, title, date_added AS dateAdded, R18 AS r18
          FROM tb_movie";
$result = $mysqli->query($query);
if($result && $result->num_rows > 0){
    
    $list = array();

    while($row = $result->fetch_assoc()){
        array_push($list, $row);
    }

    header('Content-Type: text/json');
    echo json_encode($list);
}
else{
    
}

?>