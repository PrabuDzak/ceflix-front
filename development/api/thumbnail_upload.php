<?php

require_once('src/uploader.php');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if(isset($_POST['submit'])){
        if(!empty($_FILES['thumbnail'])){
            $uploader = new Uploader($_FILES['movie']);

            $uploader->setAllowedMime(array('mp4'));
            $uploader->setFilename('movie');
            $uploader->setDestination(join(DIRECTORY_SEPARATOR, array('storage', 'movie', $callback->getId())));
            $uploader->setCallback($callback);
            $uploader->upload();
        }
    }
}

?>