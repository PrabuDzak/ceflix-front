<?php

class SRTConverter{
    protected $path;
    protected $subBatch = array();

    public function __construct($path){
        $this->path = $path;
        $pointer = 1;
        
        $lines = file($this->path);
        $sub = new stdClass;
        foreach($lines as $key => $line){
            if(strpos($line, ' --> ') !== false){
                $times = explode(' --> ', trim($line));
                $sub->id = $pointer;
                $sub->startTime = $times[0];
                $sub->endTime = $times[1];
            }
            else if(trim($line) == ""){
                array_push($this->subBatch, $sub);
                $pointer++;
                $sub = new stdClass;
            }
            else{
                if (!isset($sub->text)){
                    $sub->text = array();
                }
                array_push($sub->text, strip_tags(trim($line)));
            }
        }
    }

    public function getJSON(){
        return json_encode($this->subBatch);
    }

    public function printJSON(){
        header("Content-type: text/json");
        echo $this->getJSON();
        exit();
    }

    // class Subtitle{
    //     public $id;
    //     public $startTime;
    //     public $endTime;
    //     public $text = array();
    // }
}

?>