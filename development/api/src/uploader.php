<?php 

require_once('mysql_object.php');

interface IUploadCallback{
    public function execute();
}

class Uploader{

    protected $files = array();

    protected $root;
    protected $destination;
    protected $filename;

    protected $mime = array();

    protected $callback;

    protected $file;
    
    public function __construct($files){

        $this->root = $_SERVER['DOCUMENT_ROOT'];

        $this->files = $files;
    }

    public function setDestination($destination){
        $this->destination = $destination;
    }

    public function setFilename($filename){
        $this->filename = $filename;
    }

    public function setCallback($callback){
        $this->callback = $callback;
    }

    public function setAllowedMime($mimeArray){
        $this->mime = $mimeArray;
    }

    public function validate(){
        // INIT FILE INFO
        $this->setFileInfo();

        // CHECK MIME TYPE
        $this->checkMimeType();

        // CHECK FILE SIZE

        // FORCE OVERWRITE DUPLICATE

        return true;
    }

    public function upload(){
        if($this->validate()){
            if(move_uploaded_file($this->file['tmp_name'], $this->file['fullname'])){

                if(!empty($this->callback)){
                    $this->callback->execute();
                }

                return true;
            }
            else{
                echo "HAHA SOKOR GAK ISO UPLOAD";
            }
        }        

        return false;
    }

    protected function setFileInfo(){
        $this->file = array(
            'original_name'     => pathinfo($this->files['name'], PATHINFO_FILENAME),
            'tmp_name'          => $this->files['tmp_name'],
            'mime'              => $this->getMimeType(),
            'fullname'          => $this->getFullname()
        );
    }

    protected function getFullname(){
        $dir = join(DIRECTORY_SEPARATOR, array($this->root, $this->destination));
        if(!is_dir($dir)){
            mkdir($dir, 0777, true);
        }
        return join(DIRECTORY_SEPARATOR, array($dir, $this->filename .".". $this->getMimeType()));
    }

    protected function checkMimeType(){
        if(!empty($this->mime)){
            return in_array($this->file['mime'], $this->mime);
        }
        return true;
    }

    protected function getMimeType(){
        return pathinfo($this->files['name'], PATHINFO_EXTENSION);
    }
}

class MovieUploadStrategy implements IUploadCallback{

    protected $id;
    protected $title;
    protected $r18;

    public function __construct($id, $title, $r18){
        $this->id = $id;
        $this->title = $title;
        $this->r18 = $r18;
    }

    public function execute(){
        global $mysqli;
        $id = $this->id;
        $title = $this->title;
        $r18 = $this->r18 ? 1 : 0;
        $mysqli->query("INSERT INTO tb_movie(id, title, R18) VALUES ($id, '$title', $r18);");
        return $mysqli->query("SELECT * FROM tb_movie WHERE title = '$title'")->fetch_assoc();
    }

    public function getId(){
        return $this->id;
    }
}

class SubtitleUploadStrategy implements IUploadCallback{

    protected $movieId;
    protected $languageId;
    protected $subJSON;

    public function __construct($movieId, $languageId, $converter){
        $this->movieId = $movieId;
        $this->languageId = $languageId;
        $this->subJSON = $converter->getJSON();
    }

    public function execute(){
        global $mysqli;
        $sub = $mysqli->real_escape_string($this->subJSON);
        return $mysqli->query("REPLACE INTO tb_subtitle(movie_id, language_id, subtitle_object) VALUES ($this->movieId, $this->languageId, '$sub');");
    }

    public function getMovieId(){
        return $this->movieId;
    }
}

?>