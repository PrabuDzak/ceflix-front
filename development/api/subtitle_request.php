<?php 

require_once('src/mysql_object.php');

if(isset($_GET['movie_id']) && isset($_GET['language_id'])){
    $movieId = $_GET['movie_id'];
    $languageId = $_GET['language_id'];
    $query = "SELECT * FROM tb_subtitle WHERE movie_id = $movieId AND language_id = $languageId";

    if ($result = $mysqli->query($query)) {

        /* fetch associative array */
        $row = $result->fetch_assoc();
        header('Content-Type: text/json');
        printf ("%s \n", $row["subtitle_object"]);

        $result->free();
    }
}

?>