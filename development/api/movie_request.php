<?php 

require_once('src/mysql_object.php');
header('Content-Type: text/json');

if(isset($_GET['movie_id'])){
    
    $movieId = $_GET['movie_id'];

    $movieQuery = "SELECT id, series_id AS seriesId, title, date_added AS dateAdded, R18 AS r18
                   FROM tb_movie 
                   WHERE id = $movieId";
    $movie = $mysqli->query($movieQuery);
    if($movie && $movie->num_rows > 0){

        $info = (object)$movie->fetch_assoc();

        $subtitlesQuery = "SELECT language_id AS id, language, subtitle_object AS subtitle
                           FROM tb_subtitle, tb_subtitle_language 
                           WHERE tb_subtitle.language_id = tb_subtitle_language.id
                           AND movie_id = $info->id";
        if($subtitles = $mysqli->query($subtitlesQuery)){
            
            $info->subtitles = array();
            
            while($subtitle = $subtitles->fetch_assoc()){

                if(isset($subtitle['subtitle'])){
                    $subtitle['subtitle'] = json_decode($subtitle['subtitle']);
                }

                array_push($info->subtitles, $subtitle);
            }
        }

        echo json_encode($info);
    }
}
else{
    echo '{ error: { message: "No id is set" }';
}


?>