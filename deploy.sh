tar -czvf www.tar.gz production
scp www.tar.gz high@10.122.1.175:/var/www/ceflix.com/
ssh high@10.122.1.175 'cd /var/www/ceflix.com/; tar -xzvf www.tar.gz; mv -f --backup=numbered production/** public_html'
rm -rf www.tar.gz
