tar -czvf nos.tar.gz production --exclude='storage'
cp nos.tar.gz /opt/lampp/htdocs/ceflix/

cd /opt/lampp/htdocs/ceflix/
tar -xzvf nos.tar.gz
mv -f --backup=numbered production/** ./
rm -rf nos.tar.gz production
rm -rf *~1~*

cd ~/ceflix-front/
rm -rf nos.tar.gz